import sys, logging, re

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
sh = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('[%(asctime)s] %(levelname)s [%(filename)s.%(funcName)s:%(lineno)d] %(message)s', datefmt='%a, %d %b %Y %H:%M:%S')
sh.setFormatter(formatter)
log.addHandler(sh)

class file_validator:
    def __init__(self, contents) -> None:
        self._contents = contents
        self._validate_file()

    def __del__(self) -> None:
        pass

    def _validate_timeout(self, timeout) -> None:
        log.debug(f'Validating timeout type')
        if isinstance(timeout, int):
            log.debug(f'Timeout type is int')
            return
        raise file_error("Timeout value cannot be interpreted as an integer")

    def _validate_data_type(self, data) -> None:
        log.debug(f'Validating data type')
        if isinstance(data, str):
            log.debug(f'Data type is str')
            return
        raise file_error("File data not stored as string")

    def _validate_data_contents(self, data) -> None:
        log.debug(f'Validating data is an IP')
        if re.match("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", data):
            log.debug(f'Data is indeed an IP address')
            return
        raise file_error("Data stored in file is not a valid IPv4 address")

    def _validate_file(self) -> None:
        contents = self._contents.split('\n')

        self._validate_timeout(int(contents[0]))
        self._validate_data_type(str(contents[1]))
        self._validate_data_contents(contents[1])

class file_error(Exception):
    pass