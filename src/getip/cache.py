import sys, logging
from getip.fileraii import FileRAII
import time

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
sh = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('[%(asctime)s] %(levelname)s [%(filename)s.%(funcName)s:%(lineno)d] %(message)s', datefmt='%a, %d %b %Y %H:%M:%S')
sh.setFormatter(formatter)
log.addHandler(sh)

def cache(cache_name: str, timeout: int) -> callable:
    def decorator(fn: callable) -> callable:
        def wrapper(*args, **kwargs) -> str:
            file = FileRAII(cache_name + '.txt')
            content = file.read_lines()
            result = ''
            timeinteger = int(time.time())
            timestring  = str(timeinteger)

            if content[0] == "Error":
                return content[1]

            if timeinteger - int(content[0]) > timeout:
                log.debug(f'Cache expired')
                result = fn(*args, **kwargs)
                file.write_clean_file([timestring, result])
            else:
                log.debug(f'Using cached data')
                result = '\n'.join(content[1:])
            return result

        return wrapper
    return decorator

def main() -> None:
    pass

if __name__ == '__main__':
    main()