import os, sys, logging
from urllib.request import Request, urlopen
from urllib.error import URLError
from configparser import ConfigParser
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from getip.cache import cache

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
sh = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('[%(asctime)s] %(levelname)s [%(filename)s.%(funcName)s:%(lineno)d] %(message)s', datefmt='%a, %d %b %Y %H:%M:%S')
sh.setFormatter(formatter)
log.addHandler(sh)

config = ConfigParser()
config.read('config.ini')

'''
    Get the external IP from icanhazip.com. The webtool has a limit of requests to 
    once every minute. The cache decorator makes sure we only request once every 
    minute, or else load stored value from file
'''

def httpget(url: str, headers: dict = None) -> str:
    try:
        log.debug(f'Requesting from URL {url}')
        
        if headers is not None:
            log.debug(f'Using headers {repr(headers)}')

        request = Request(url, headers=headers)
        contents = urlopen(request).read()
    except URLError as urlerror:
        log.error(str(urlerror))
        return str(urlerror)
    return contents.decode('utf-8').strip()

@cache(config.get('cache', 'file_name'), int(config.get('cache', 'timeout')))
def getIp() -> str:
    log.debug(f'Getting IP')
    return httpget(config.get('ip_service', 'url'), {'User-Agent': config.get('ip_service', 'user_agent').strip('\"')})

def main() -> None:
    print(getIp())

if __name__ == '__main__':
    main()

    
