import sys, logging
from getip.file_validation import file_validator, file_error

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
sh = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('[%(asctime)s] %(levelname)s [%(filename)s.%(funcName)s:%(lineno)d] %(message)s', datefmt='%a, %d %b %Y %H:%M:%S')
sh.setFormatter(formatter)
log.addHandler(sh)

class FileRAII:
    def __init__(self, fname: str) -> None:
        try:
            log.debug(f'Opening file {fname}')
            self._f = open(fname, 'r+')
        except FileNotFoundError:
            log.warning(f'File not found, trying to create')
            with open(fname, 'w') as f:
                f.write('0\n127.0.0.1')
            self._f = open(fname, 'r+')

    def __del__(self) -> None:
        log.debug(f'Closing file')
        self._f.close()

    def read_lines(self) -> list:
        log.debug(f'Reading file')
        contents = self._f.read()

        try:
            _ = file_validator(contents)
        except file_error as e:
            log.error('Could not validate cache file')

        return contents.split('\n')

    def write_clean_file(self, lines: list) -> None:
        log.debug(f'Truncating and writing to file')
        self._f.seek(0)
        self._f.write('\n'.join(lines))
        self._f.truncate()

def main() -> None:
    pass

if __name__ == '__main__':
    main()