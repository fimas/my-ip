from getip.main import main,getIp
import re

def test_main() -> None:
    assert main() is None
    assert isinstance(getIp(), str)
    assert re.match("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", getIp())