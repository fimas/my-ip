# My IP

Get the external IP from icanhazip.com. The webtool has a limit of requests to 
once every minute. The cache decorator makes sure we only request once every 
minute, or else load stored value from file
